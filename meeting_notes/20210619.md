# WSeaLUG Bi-Weekly Meeting
## Date: 2021-05-08

# Topics

## Software Defined Radio

  * https://greatscottgadgets.com/hackrf/one/
  * https://github.com/mossmann/hackrf/wiki/HackRF-One
  * https://www.gnuradio.org/
  * https://greatscottgadgets.com/sdr/ (SDR 101) 
  * How can SDR be used to see what is around but not visable i.e wifi, survellience? 
  * `#wsealug-radio:seattlematrix.org`
  * https://meteospace.com/receiving-noaa-satellite-weather-images-with-a-rtl-sdr/
  * https://web.archive.org/web/20111210051239/http://www.nearfield.org/2011/02/wifi-light-painting

## Raspberry Pi Projects

  * https://www.nzyme.org/
  * https://www.home-assistant.io/
  * https://www.adafruit.com/product/4393
  * https://retropie.org.uk/
  * https://piwebcam.github.io/
  * https://octoprint.org
 
## Home Assistant

  * https://www.home-assistant.io/
  * https://www.zwaveproducts.com/
  * https://buildwithmatter.com/
  * https://yeelight.readthedocs.io/
  
## HTTP Load Generator and Testing

  * https://github.com/rakyll/hey
  * https://httptoolkit.tech/
    * https://github.com/httptoolkit
  * https://www.charlesproxy.com/
  * https://github.com/fox-it/mitm6
  * https://mitmproxy.org/

## Note Taking

**Zettelkasten**

  * https://en.wikipedia.org/wiki/Zettelkasten
  * https://github.com/zettel-kasten/zettelkasten
  * https://www.reddit.com/r/Zettelkasten/comments/b566a4/what_is_a_zettelkasten/
  * https://www.jhonatandasilva.com/published/1623367665
  * https://medium.com/@rebeccawilliams9941/the-zettelkasten-method-examples-to-help-you-get-started-8f8a44fa9ae6

**General Methods/Apps**

  * OneNote
  * Evernote
  * Pile-o-textfiles
  * mkdocs + notes git repo: https://www.mkdocs.org/
  * cherrytree - https://github.com/giuspen/cherrytree 
  * https://tiddlywiki.com/
  * github private gist
  * https://turtlapp.com/
