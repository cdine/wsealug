## WSeaLUG Meeting 

2021-06-05 

## Best Web Browser for privacy
- https://spyware.neocities.org/articles/firefox.html
- https://librewolf-community.gitlab.io/
- https://digdeeper.neocities.org/ghost/browsers.html

Pale Moon is still the only decent way to browse the modern web that's actually relevant - but it's slowly rotting from the inside. Firefox is dying and will soon bring down all its forks alongside itself, surrendering the Web to Google whose abomination of a browser is just as worthless. Promising projects such as Otter Browser or suckless surf suffer from small dev teams, no / low addon support and don't have their own engines - so depend on Google / Apple, anyway. The only reasonable choice is Pale Moon until Web Browser gets more support. Or, just try wean yourself off the modern web by sticking to websites such as the ones on Neocities, wiby.me, etc. which are functional in NetSurf or terminal browsers. I hate to kill the positivity of yet another summary, but if reality forces me to - what can I do?

- http://www.palemoon.org/

## Amazon Sidewalk?
- BAD IDEA!
https://www.wired.com/story/how-amazon-sidewalk-works/
https://www.cnet.com/home/smart-home/amazon-sidewalk-will-create-entire-smart-neighborhoods-faq-ble-900-mhz/

## What to Binge?
- Startup (Netflix & Crackle)
- Halt and Catch Fire (Netflix)
- Penny Dreadful (Netflix)
- Queens Gambit (Netflix)
- Love Death + Robots (Netflix)
- The Boys (Prime)
- There Is No Antimemetics Division (http://www.scpwiki.com/antimemetics-division-hub)
- The Black List
- My Hero Acadima (anime)
- Demon Slayer (anime)
- The Neighorhood 
- https://en.wikipedia.org/wiki/Gotham_(TV_series)
- Lucifer (netlfix)
- Upload (prime)
- The Boys (prime)
- Humans 


## Tank Man
- https://www.vice.com/en/article/qj8v9m/bing-censors-tank-man

## userChrome.css, userstyles, userscripts
- http://kb.mozillazine.org/index.php?title=UserChrome.css&printable=yes
- https://gist.github.com/AndrewKvalheim/7accb09dbddd494f53dc889877c1f524
- https://github.com/openstyles/stylus/
- https://en.wikipedia.org/wiki/Userscript