
# WSeaLUG 2021-08-14

# Attendance
- andrew
- mcginger
- mousey
- prasket
- salt
- sntxrr
- tree
- tzcrawford

# Topics

## TMUX tips and tricks

- bioboot
- https://danielmiessler.com/study/tmux/
- https://www.byobu.org/
- https://github.com/tmux-python/libtmux/
- book that was recommended to me to build my own config: https://pragprog.com/titles/bhtmux2/tmux-2/

## DNS over HTTPS for SM?

- https://twitter.com/EmeraldOnion/status/1379999801148796928
- https://dnscrypt.info/
- https://emeraldonion.org/
- BIG undertaking 

## Self-hosting Matrix Server

- "set it and forget it"
- https://github.com/spantaleev/matrix-docker-ansible-deploy
- Kubernetes via helm: https://github.com/dacruz21/matrix-chart
  * also updated regularly


## Merging SeattleMatrix and LUG Channels

- Discuss in each room

## SteamDeck

- https://www.steamdeck.com/en/

## LUG Picnic

- take it week by week

## NES on Linux

- https://www.debugpoint.com/2016/07/3-nes-emulators-to-play-old-nes-games-in-linux/

## Meeting Topics

- Running list of topics on [etherpad](https://etherpad.seattlematrix.org/p/WSeaLUG-Topics)
