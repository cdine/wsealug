# WSeaLUG Bi-Weekly Meeting
## Date: 2021-05-08


## Andrew's Mannequin-butt lamp 🤪

  * USB CO2 sensor? https://github.com/larsp/co2monitor
  * Direct to Pi https://www.adafruit.com/product/4867
  * https://dokku.com/

## Anime Watch List

  * Castlevania
  * Attach on Titan
  * Demon Slayer
  * Voltron
  * Cowboy Bebop
  * Deathnote
  * Planetes
  * Transformers
  * Tokyo Ghoul
  * Parasyte
  * One Punch Man
  * My Hero Acadamia 
  * Berserk (very adult, the first season, not the 20 year later version)
  * Trigun (probably Salt's favorite anime)
  * Ruroni Kenshin / Samurai X (omgosh, the early story arcs are great, the later ones get... underground jesus  
  * knights that can blind with light but still based in samurai era japan)
  * Vampire Hunter D
  * Paranoia Agent
  * Serial Experiments Lain
  * Ghost in the Shell
  * .hack//Sign
  * Ergo Proxy 
  * Fullmetal Alchemist
  * FLCL
  * Read or Die
  * Samurai Champloo (better cowboy bebop j/k) -- I dunno it might be :D 
  * Afro Samurai


## Games you are playing

  * Fortnite
  * Skyward Sword (coming to switch soon)
  * Obduction (from the Myst creators)
  * Minecraft

Chill Games for Relaxing:
  * A Short Hike
  * A Monster's Expedition
  * Terra Nil
  * Untitled Goose Game


## Pinecone overlay network

  * https://matrix.org/blog/2021/05/06/introducing-the-pinecone-overlay-network
  * https://github.com/yggdrasil-network/yggdrasil-go
  * https://github.com/matrix-org/dendrite/labels/are-we-synapse-yet
  * https://justanman.org/posts/the-last-message-sent-on-aim/

## Audacity's opt-in telemetry

  * https://www.audacityteam.org/
  * https://github.com/audacity/audacity/pull/835
  * https://www.sonicvisualiser.org/
