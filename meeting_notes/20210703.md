# WSeaLUG Bi-Weekly Meeting

## Date: 2021-05-08

## Topics

## Capture Card from Pizzalovingnerd for OBS

    https://www.amazon.com/BlueAVS-Capture-USB2-0-Camcorder-Extension/dp/B088HBRM7T/ref=sr_1_3?dchild=1&keywords=HDMI+Video+Capture&qid=1625328790&sr=8-3

    22$ Capture Card



## Ansible Demo by Harbinger

    https://www.ansible.com/overview/how-ansible-works

    https://github.com/harbinger55/mac_setup

    https://github.com/spantaleev/matrix-docker-ansible-deploy 

    ansible project running Seattle Matrix 


    The Fuck

    https://github.com/nvbn/thefuck -- cli tool for helping with cli typos

    installable via homebrew and in some repos


## Misc

    https://github.com/syncthing/syncthing

    https://github.com/magic-wormhole/magic-wormhole



## Salt's Ansible File Hierarchy
```
salt@sensaltey-pop:/SaltHome/Sync/All/ansible-system-setup$ ls -R
.:
altsaltk.yml  assets  backups  bootstrap.sh  NOTES.md  README.md  roles  saltemp-uw.yml  saltx390.yml

./assets:
nixpkgs  scripts  userChrome.css

./assets/nixpkgs:
config.nix  home.nix  overlays

./assets/nixpkgs/overlays:
base.nix  base.nix.orig  core.nix  desktop.nix  desktop.nix.orig  dev.nix  gnome.nix  intel.nix  laptop.nix  thinkpad.nix

./assets/scripts:
gufw

./backups:
crypttab  fstab  initramfs  zz-update-efistub

./backups/initramfs:
post-update.d

./backups/initramfs/post-update.d:
zz-update-efistub

./roles:
common  docker_host  example  nix

./roles/common:
tasks  vars

./roles/common/tasks:
configure.yml  env  finalize.yml  install.yml  main.yml  pkgs

./roles/common/tasks/env:
gnome-dconf.yml  gnome.yml  lxde.yml  users.yml

./roles/common/tasks/pkgs:
additional.yml  appstores.yml  steam.yml

./roles/common/vars:
main

./roles/common/vars/main:
packages.yml  pkg_versions.yml

./roles/docker_host:
tasks

./roles/docker_host/tasks:
main.yml

./roles/example:
defaults  files  handlers  library  lookup_plugins  meta  module_utils  tasks  templates  vars

./roles/example/defaults:

./roles/example/files:

./roles/example/handlers:

./roles/example/library:

./roles/example/lookup_plugins:

./roles/example/meta:

./roles/example/module_utils:

./roles/example/tasks:

./roles/example/templates:

./roles/example/vars:

./roles/nix:
tasks

./roles/nix/tasks:
main.yml
```


