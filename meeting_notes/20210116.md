# West Seattle LUG
## bi-weekly meeting
## date: 2021-01-16 

### How to distract the LUG - ask for a countdown timer one liner :joy: haha
- for i in $(seq 480); do echo "$((480-i))"; sleep 1; done
- for i in $(seq 180); do echo "$((180-i))"; sleep 1; done
- potentially: date --utc --date "@$((480 - i))" '+%M:%S'

## Topics

### Open Source Home Router
- https://www.turris.com/en/omnia/overview/
- https://en.wikipedia.org/wiki/PirateBox

#### Salt's terribly large link drop... (mostly about apu2e4 related things)
##### apu2.* resources
- [thread about a custom apu2 build with two mikrotik cards](https://forum.openwrt.org/t/pc-engines-apu2-recommended/56886)
- [difference between APU2/3/4](https://teklager.se/en/knowledge-base/whats-difference-between-apu2-and-apu3-boards/)
- [post about switching from ubiquiti to opnsense](https://kcore.org/2020/11/08/USG-to-OPNsense/)
- https://openwrt.org/toh/pcengines/apu2
- https://pcengines.ch/apu2e4.htm
- [ipfire official offering compared to apu2 and cheap chinese mini pcs](https://community.ipfire.org/t/mini-pc-hardware/45)
- [pfsense supporting company offering compared to apu2](https://forum.netgate.com/topic/119913/no-love-for-pc-engines-anymore)
- [discussion about apu2e4 vs sg2100, also includes qotom talk](https://www.reddit.com/r/PFSENSE/comments/jkth7a/apu2e4_vs_sg2100/)

##### all about wlan cards
- [great explanation of various wifi modes part way down](https://forum.turris.cz/t/is-the-wifi-dual-band-or-single-band/54?page=2)
- [some good info on atheros vs mediatek](https://forum.openwrt.org/t/difference-between-atheros-and-mediatek/35030)
- [comparing qca9880 and mt7612e, if qca9880 make sure to get v2 rev br4a, still from 2013, qca9984 is from 2015 and 4x4, ](https://forum.openwrt.org/t/qca9880-vs-mt7612e/33326)
- [various compex options, explanation of difference between simultanious band operation](https://forum.openwrt.org/t/compex-mini-pcie-cards-supported/31004)
- [discussion about antenna power and potential causes of interferance](https://forum.ipfire.org/viewtopic.php?t=12350)
- [2016 discussion about 3x3 ac cards](https://hardforum.com/threads/list-of-mini-pcie-3x3-ac-cards.1918807/)
- [2018 discussion about 3x3 ac cards](http://forum.notebookreview.com/threads/3x3-ac-adapters-mini-pcie.814704/)
- [discussion about 2x2 vs 3x3 and ath9 vs ath10](https://www.reddit.com/r/homelab/comments/628j8g/mini_pcie_card_for_linuxbased_simultaneous_dual/)
- [discussion about blob-free ath9k cards](https://www.reddit.com/r/linuxhardware/comments/7trlba/list_of_blobfreeopen_source_ath9k_wifi_cards/)
- [discussion about 802.11ax driver support with some potential options](https://forum.openwrt.org/t/802-11ax-wifi-ap-mpci-e-cards/63577)
- [discussion with recmmendation of higher powered qualcomm cards](https://forum.openwrt.org/t/ac-mini-pcie-card-recommendation/9682)

###### potentially problematic concerns
- [GH issue about some PCIe cards not being detected on an APU2](https://github.com/pcengines/apu2-documentation/issues/115)
- [description of ap7620 oversize](http://linuxgizmos.com/openwrt-router-board-installs-in-a-mini-pcie-slot/)
- [discussion of full minipcie cards needing external amps and maybe not working with apu2e4](https://forum.openwrt.org/t/available-mt76-based-minipcie-cards/58878)
- [discussion of 802.11ax mt7915 or QCN9074](https://forum.openwrt.org/t/ap-mode-wifi6-ax-pcie-card/68466)
- [bleak outlook for apu2e4 supporting wifi6](https://forum.openwrt.org/t/apu2-openwrt-x86-64-and-wifi-6/83574)
- [bsd/pfsense not supporting 802.11ac...](https://docs.netgate.com/pfsense/en/latest/wireless/hardware.html#cards-supporting-access-point-hostap-mode)
- [why to be warry of intel cards, no ap support](https://unix.stackexchange.com/questions/598275/intel-ax200-ap-mode)
- [long HN thread about MikroTik routers forwarding traffic to unkown places](https://news.ycombinator.com/item?id=17908028)

##### potential wlan chips
- http://trac.gateworks.com/wiki/wireless/wifi
- https://web.archive.org/web/*/https://wikidevi.com/wiki/List_of_Four_Stream_Hardware#Wireless_Adapters
- https://web.archive.org/web/*/https://wikidevi.com/wiki/List_of_Three_Stream_Hardware#Wireless_Adapters
- https://web.archive.org/web/*/https://wikidevi.com/wiki/List_of_802.11ac_Hardware
- https://web.archive.org/web/*/https://wikidevi.com/wiki/List_of_802.11ax_Hardware
- [confirmed no closed-source firmware cards](https://www.thinkpenguin.com/catalog/wireless-networking-gnulinux)
- [potentially interesting concurent card launched in septemberish 2020](http://forum.banana-pi.org/t/802-11ac-4x4-standard-size-mini-pcie-card-is-launched/11545)
- https://compex.com.sg/shop/wifi-module/802-11ac-wave-2/wle1216vx-2/
- https://www.asiarf.com/shop/wifi-wlan/wifi_mini_pcie/wifi-mini-pcie-module-mtk-mt7615-4x4-11ac-5g-ws3294-manufacturer/
- https://www.asiarf.com/shop/wifi-wlan/wifi_mini_pcie/wifi-11ac-full-mini-pcie-card-mt7615-4t4r-dbdc-1-7g-aw7615-np1/
- https://www.asiarf.com/shop/hot-sales/wifi6-4t4r-dual-bands-selectable-mpcie-card-ieee802-11ax-ac-a-b-g-n-2-4g-5ghz-aw7915-np1/
- [wmx6401 11ac 4x4 wave2](https://www.jjplus.com/wmx6401/)
- https://www.524wifi.com/index.php/catalog/product/view/id/251/s/qualcomm-wle900vx-7a-minipcie-module-qca9880-802-11ac-2-4-5ghz-3-3-mimo/
- https://compex.com.sg/wle1200v2-22/

##### off the shelf solutions/additions
- https://www.turris.com/en/omnia/overview/
- https://fit-iot.com/web/products/fitlet2/
- https://mikrotik.com/product/cap_ac
- https://www.ui.com/unifi/unifi-ap/

###### Raspberry Pi 4 specs
- SoC: Broadcom BCM2711B0 quad-core A72 (ARMv8-A) 64-bit @ 1.5GHz
- GPU: Broadcom VideoCore VI
- Networking: 2.4 GHz and 5 GHz 802.11b/g/n/ac wireless LAN
- RAM: 1GB, 2GB, or 4GB LPDDR4 SDRAM
- Bluetooth: Bluetooth 5.0, Bluetooth Low Energy (BLE)
- GPIO: 40-pin GPIO header, populated
- Storage: microSD
- Ports: 2 × micro-HDMI 2.0, 3.5 mm analogue audio-video jack, 2 × USB 2.0, 2 × USB 3.0, Gigabit Ethernet, Camera Serial Interface (CSI), Display Serial Interface (DSI)
- Dimensions: 88 mm × 58 mm × 19.5 mm, 46 g


### zsh transition from bash
- OHMyZHShell?

#### Salt's terribly large link drop... (mostly about choosing zinit)
##### reddit conversations...
- [speed comparison of plugin managers](https://www.reddit.com/r/zsh/comments/ak0vgi/a_comparison_of_all_the_zsh_plugin_mangers_i_used/)
- [explanation of choosing zinit after speed comparison](https://www.reddit.com/r/zsh/comments/ak0vgi/a_comparison_of_all_the_zsh_plugin_mangers_i_used/ef11che/)
- [explanation of searching for plugin managers and landing on zinit](https://www.reddit.com/r/zsh/comments/9yuqd3/zplugin_or_antibody/ebzwzfj/)
- [brief explanation of why zinit is so fast and potential future uses](https://www.reddit.com/r/zsh/comments/dipf92/zgen_vs_prezto/f3xhnwp/)
- [author of zinit talking about why it isn't too complex](https://www.reddit.com/r/zsh/comments/ah17q7/zplugin_is_nightmarishly_complex_but_is_there/eef552r/)
- [author of zinit  talking about antigen being bad](https://www.reddit.com/r/zsh/comments/536rdy/want_to_try_zsh_again_what_about_antibody/d7rj50d/)
- [argument against using plugin managers](https://www.reddit.com/r/zsh/comments/ah17q7/zplugin_is_nightmarishly_complex_but_is_there/etjr8m8/)
- [link to thorough and well-documented hand-config](https://www.reddit.com/r/zsh/comments/ah17q7/zplugin_is_nightmarishly_complex_but_is_there/eeanr85/)

#####  [zinit](https://github.com/zdharma/zinit)
###### [zinit config examples](https://github.com/zdharma/zinit-configs)
- https://zdharma.org/zinit/wiki/Example-Minimal-Setup/
- https://zdharma.org/zinit/wiki/Example-Oh-My-Zsh-setup/
- https://zdharma.org/zinit/wiki/GALLERY/
- [minimal content and style](https://github.com/zdharma/zinit-configs/blob/master/jubi/.zshrc)
- [well documented, loads some omz](https://github.com/zdharma/zinit-configs/blob/master/numToStr/zshrc.zsh)
- [super full, modular, well documented](https://github.com/zdharma/zinit-configs/tree/master/lainiwa/2nd_config)
- [full, modular, well documented, has p10k config, lots of custom work, creator of zinit](https://github.com/zdharma/zinit-configs/tree/master/psprint)
- [fairly large, minimal documentation, collapsed syntax](https://github.com/zdharma/zinit-configs/blob/master/brucebentley/zshrc.zsh)
- [examples with ksh and bash, but not really a fan of the formatting](https://github.com/zdharma/zinit-configs/tree/master/agkozak)
- [very full, modular, has patch examples, bit confusing](https://github.com/zdharma/zinit-configs/tree/master/NICHOLAS85) 
- [entire project, but based on zinit](https://github.com/black7375/BlaCk-Void-Zsh)

###### misc references
- https://zdharma.org/zinit/wiki/INTRODUCTION/
- https://zdharma.org/zinit/wiki/crasis/
- https://www.reddit.com/r/zinit/
- https://www.reddit.com/r/zplugin/

##### always more lists and references...
- [hand configured zsh, lots of comments](https://github.com/cbarrick/dotfiles/blob/master/home/.zsh/.zshrc)
- [minimal hand configs, zsh-pony](https://github.com/mika/zsh-pony)
- [potentially good configs to pull from, sneaky branch installation](https://github.com/romkatv/zsh4humans)
- [tips and tricks](https://strcat.de/zsh/#tipps)
- https://wiki.archlinux.org/index.php/zsh
- https://project-awesome.org/unixorn/awesome-zsh-plugins
- https://github.com/k4m4/terminals-are-sexy


### Synology
- Linux sinner 4.4.59+ #25426 SMP PREEMPT Mon Dec 14 18:48:50 CST 2020 x86_64 GNU/Linux synology_apollolake_1019+
- https://www.fosslinux.com/43258/best-linux-nas-solutions.htm
- https://www.openmediavault.org/ • NAS based on Debian


### Upgrading SeattleMatrix
- how old is our version of etherpad to not support md?
- can we try https://hedgedoc.org/
- matrix server having memoryleak issues..Might had been sqllite issues, have upgraded to all Postgres
- offloading postgres to DO paas
- https://github.com/containrrr/watchtower


### Honeypots
- https://haas.nic.cz/ (same company behind turris, pcengines)


## Notes
- https://codegolf.stackexchange.com/


