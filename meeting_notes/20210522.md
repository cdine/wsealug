# WSeaLUG Meeting - 20210522

https://datatracker.ietf.org/doc/html/rfc3339
https://www.seattletimes.com/business/for-haggis-purists-in-the-u-s-the-law-is-no-obstacle/

## freenode

- https://gist.github.com/joepie91/df80d8d36cd9d1bde46ba018af497409
- https://news.ycombinator.com/item?id=27237202
- https://news.ycombinator.com/item?id=27235432
- https://news.ycombinator.com/item?id=27235565
- https://ariadne.space/2021/05/20/the-whole-freenode-kerfluffle/
- https://www.hackread.com/private-internet-access-pia-vpn-sold-israel-privacy-concerns/
- https://telegra.ph/Private-Internet-Access-VPN-acquired-by-malware-business-founded-by-former-Israeli-spies-12-01
- https://libera.chat/
- https://github.com/SeaGL/organization/issues/125
- https://github.com/matrix-org/matrix-appservice-irc/issues/1324


## IRL meetings

- hybrid? temp or perm?
- portal, so non-local can maintain connection
- https://www.seattle.gov/parks/reserve/picnic-reservations


## SeattleMatrix

- https://wekan.seattlematrix.org/b/rzJSLP3Qb4TzP5Qdb/seattlematrix-first-2021-meeting
- probably best to combine everything except for jitsi
- jitsi needs a lot of bandwidth and is working well on one of sntxrr's servers
- meeting should happen soon to figure out next steps and then get going
- everyone seems most available after wsealug meetings


## adding keyboard layout links, even if we don't cover the topic...

- https://github.com/MadRabbit/halmak#comparisons
- http://nikolay.rocks/2016-12-20-the-halmak-reborn
- http://microexploitation.com/2018/06/04/thinqu/
- http://mkweb.bcgsc.ca/carpalx/?full_optimization
- colemak-dh (biggest community?) https://colemakmods.github.io/mod-dh/
- learning to type in new layout: https://www.keybr.com/
- https://github.com/MadRabbit/keyboard-genetics
- select by "just feels good on your fingers"
- more recent design is probably better than traditional
- https://andrew.kvalhe.im/2021-03-12T17:36:48
- https://andrew.kvalhe.im/2021-04-09T08:50:38
- https://andrew.kvalhe.im/+z9teykdkob7xyheukj7xbko1sz8rwt4b
- https://github.com/sezanzeb/key-mapper
- https://forum.colemak.com/topic/2552-i-developed-a-new-one-handed-layout-streak/


