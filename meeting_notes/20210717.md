# WSeaLUG - Saturday, July 17, 2021 
 
## Setting up vault for Seattlematrix - single Docker host or multiple hosts?
- https://www.vaultproject.io/

## Mailserver for SM - what one?
- https://gitlab.com/seattlematrix/seattlematrix/-/issues/16
- links??
- https://github.com/JojiiOfficial/Matrix-EmailBridge

## How to handle archived emails?
- https://www.pcvita.com/blog/view-thunderbird-email-as-html.html
    - can export Thunderbird to HTML or plain text 

## What to put on your RaspberryPi
- https://heimdall.site/
- https://www.nzyme.org/
- plants
- https://andrew.kvalhe.im/2011-08-11T11:29:03
- https://andrew.kvalhe.im/2011-08-25T09:38:27
- https://andrew.kvalhe.im/2011-08-25T10:09:53

## NeoVIM
- Dedicated navigation keys: https://andrew.kvalhe.im/📌b806c3bd
